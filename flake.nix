{
  description = "My fork of the st terminal emulator";

  outputs = { self }: {
    overlay =
      final: prev: {
        st = prev.st.overrideAttrs (super: {
          src = self;
          buildInputs = super.buildInputs ++ [ prev.harfbuzz ];
        });
      };
  };
}
